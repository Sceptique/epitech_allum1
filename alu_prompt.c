/*
** alu_prompt.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb  7 18:41:39 2014 poulet_a
** Last update Sat Feb 15 11:24:23 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"
#include "mytermios.h"
#include "myalu.h"
#include "myselect.h"

/*
** my_putnbr(size);
** my_putstr(" : ");
*/
int	alu_put_one(t_list *l, char current)
{
  int	i;
  long	size;

  RET_NULL_LONE(l);
  size = (long)l->e;
  if (current)
    termios_put_reverse_enable();
  i = 0;
  while (i++ < (alu_get_size_last_line() - size) / 2)
    my_putstr(" ");
  i = 0;
  while (i++ < size)
    my_putchar('|');
  my_putstr("\n");
  if (current)
    return (termios_put_none());
  return (0);
}

int		alu_prompt()
{
  t_list	*l;
  int		id;

  if (alu_manage(NULL) == NULL)
    RET_NULL_LONE((alu_manage(alu_init(alu_set_nb_lines(0)))));
  termios_put_clear();
  my_putstr("CURRENT ACTION >> YOU ARE TAKING : ");
  my_putnbr(alu_rank_change(0));
  my_putstr(" ALLU(s).\n");
  l = alu_manage(NULL);
  id = 0;
  while (l)
    {
      alu_put_one(l, my_select(0, alu_set_nb_lines(0)) == id);
      id++;
      l = l->next;
    }
  return (id);
}
