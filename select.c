/*
** select.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Tue Feb  4 13:30:51 2014 poulet_a
** Last update Sat Feb 15 11:28:47 2014 poulet_a
*/

#include <curses.h>
#include "myselect.h"
#include "myalu.h"
#include "mygame.h"

int		my_select_enter(int id)
{
  t_list	*alu;

  RET_NULL_LONE((alu = list_index(alu_manage(NULL), id)));
  if ((long)(alu->e) <= 0)
    {
      alu_rank_reset();
      return (-1);
    }
  alu_remove_id(alu_manage(NULL),
		my_select(0, alu_set_nb_lines(0)));
  game_play(1);
  if (game_play(0) != -1)
    alu_prompt();
  game_play(1);
  return (id);
}

int		my_select(int key, int lin_nb)
{
  static int	id = - 1;

  if (id == -1)
    id = alu_set_nb_lines(0) - 1;
  if (key == 0)
    return (id);
  else if (key == KEYBOARD_LESS || key == KEYBOARD_SUPPR)
    alu_rank_change(1);
  else if (key == KEYBOARD_RETURN)
    alu_rank_reset();
  else if (key == KEYBOARD_ENTER && alu_rank_change(0))
    my_select_enter(id);
  else if (alu_rank_change(0) != 0)
    return (-1);
  else if (key == KEYBOARD_DOWN)
    my_select_next(&id, lin_nb);
  else if (key == KEYBOARD_UP)
    my_select_prev(&id, lin_nb);
  return (-1);
}

void		my_select_next(int *id, int size)
{
  *id = (*id + 1) % size;
}

void		my_select_prev(int *id, int size)
{
  *id = (*id - 1) % size;
  if (*id < 0)
    *id = *id + size;
}
