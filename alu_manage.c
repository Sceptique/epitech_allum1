/*
** alu_manage.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sun Feb  9 16:59:22 2014 poulet_a
** Last update Tue Feb 11 10:31:55 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"
#include "myalu.h"

t_list		*alu_manage(t_list *alu)
{
  static t_list	*save = NULL;

  if (alu != NULL)
    return ((save = alu));
  else
    return (save);
}

int	alu_count(t_list *alu)
{
  int	nb;

  nb = 0;
  while (alu != NULL)
    {
      nb += (long)alu->e;
      alu = alu->next;
    }
  return (nb);
}

int		alu_rank_change(int i)
{
  static int	nb = 0;

  nb = nb + i;
  if (nb < 0)
    nb = 0;
  return (nb);
}

void     alu_rank_reset()
{
  alu_rank_change(-alu_rank_change(0));
}

