##
## Makefile for  in /home/poulet_a/projets/allum1-2018-poulet_a
## 
## Made by poulet_a
## Login   <poulet_a@epitech.net>
## 
## Started on  Mon Feb  3 10:51:14 2014 poulet_a
## Last update Sat Feb 15 12:53:19 2014 poulet_a
##

CC	=	cc

RM	=	rm -f

CFLAGS  +=      -Ilibs -Llibs -lmy -lmytermios
CFLAGS	+=	-Wall -Wextra -Wextra -pedantic
CFLAGS	+=	-lncurses

NAME	=	allum1

SRCS	=	main.c \
		select.c \
		alu_prompt.c \
		alu_manage.c \
		alu.c \
		game.c

OBJS	=	$(SRCS:.c=.o)

LIB_STD =	std_epitech
LIB_TER =	term_epitech

all:		libs $(NAME)

libs:
		@(cd $(LIB_STD) && $(MAKE))
		@(cd $(LIB_TER) && $(MAKE))
		@(cd $(LIB_STD) && $(MAKE) libs)
		@(cd $(LIB_TER) && $(MAKE) libs)

$(NAME):	$(OBJS)
		cc $(OBJS) -o $(NAME) $(CFLAGS)

clean:
		@(cd $(LIB_STD) && $(MAKE) $@)
		@(cd $(LIB_TER) && $(MAKE) $@)
		$(RM) $(OBJS)

fclean:		clean
		@(cd $(LIB_STD) && $(MAKE) $@)
		@(cd $(LIB_STD) && $(MAKE) rmlibs)
		@(cd $(LIB_TER) && $(MAKE) $@)
		@(cd $(LIB_TER) && $(MAKE) rmlibs)
		$(RM) $(NAME)

re:		fclean all
		@(cd $(LIB_STD) && $(MAKE) relibs)
		@(cd $(LIB_TER) && $(MAKE) relibs)

.PHONY:		clean fclean all re libs
