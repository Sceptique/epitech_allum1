/*
** alu.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Fri Feb  7 18:41:12 2014 poulet_a
** Last update Sat Feb 15 11:29:46 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"
#include "myalu.h"

int		alu_set_nb_lines(int n)
{
  static int	save_n = 4;

  if (n != 0)
    save_n = n;
  if (save_n > 10)
    save_n = 10;
  return (save_n);
}

int	alu_get_size_last_line()
{
  return ((alu_set_nb_lines(0) - 1) * 2 + 1);
}

t_list		*alu_init(long col)
{
  t_list	*alu;
  long		i;

  alu = NULL;
  i = 0;
  while (i < col)
    {
      alu = list_create(alu, (void*)(i * 2 + 1));
      i++;
    }
  return (alu);
}

int		alu_remove_id(t_list *alu, int id)
{
  t_list	*tmp;

  RET_NULL_LONE(alu);
  RET_NULL_LONE((tmp = list_index(alu, id)));
  if ((long)tmp->e == 0)
    return (-1);
  tmp->e = (void*)((long)tmp->e - alu_rank_change(0));
  if ((long)tmp->e < 0)
    tmp->e = 0;
  alu_rank_reset();
  return (0);
}
