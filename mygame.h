/*
** mygame.h for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sun Feb  9 17:04:43 2014 poulet_a
** Last update Fri Feb 14 15:50:33 2014 poulet_a
*/

#ifndef MYGAME_H_
# define MYGAME_H_

int	game_over(int is_player);
int	game_play(int action);
int	game_ia_easy();
int	game_ia_hard();

#endif /* !MYGAME_H_ */
