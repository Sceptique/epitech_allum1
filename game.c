/*
** game.c for  in /home/poulet_a/projets/allum1-2018-poulet_a
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Sun Feb  9 17:01:54 2014 poulet_a
** Last update Fri Feb 14 21:52:16 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"
#include "myalu.h"
#include "mygame.h"

int	game_over(int is_player)
{
  if (is_player != -1 && alu_count(alu_manage(NULL)) <= 0)
    {
      if (is_player)
	my_putstr("Game over boy.\n");
      else
 	my_putstr("You won the game.\n");
      return (game_play(-1));
    }
  return (0);
}

int		game_play(int action)
{
  static int	player = 1;

  if (action == -1)
    player = -1;
  if (player == -1)
    return (-1);
  if (player == 1)
    {
      if (action == 1)
	{
	  player = 0;
	}
    }
  else if (player == 0)
    {
      if (action == 1)
	{
	  player = 1;
	  RET_LONE_LONE(game_ia_hard());
	}
    }
  game_over(player == 0);
  return (player);
}

/*
** the first alumet it finds
*/
int		game_ia_easy()
{
  t_list	*alu;

  RET_NULL_LONE((alu = alu_manage(NULL)));
  while (alu && (long)alu->e < 1)
    alu = alu->next;
  RET_NULL_LONE(alu);
  alu->e = (void*)((long)(alu->e) - 1);
  return (0);
}

int		game_ia_hard()
{
  t_list	*alu;
  int		v;

  RET_NULL_LONE((alu = alu_manage(NULL)));
  v = 0;
  while (alu && (alu = alu->next))
    v = v ^ (long)alu->e;
  RET_NULL_LONE((alu = alu_manage(NULL)));
  if ((v & 1) == 1)
    {
      while (alu && (long)alu->e < 2)
	alu = alu->next;
      if (alu != NULL && (alu->e = (void*)(0)) == (void*)(0));
      else
	return (game_ia_easy());
    }
  else
    {
      while (alu && (long)alu->e < 2)
	alu = alu->next;
      if (alu != NULL && (alu->e = (void*)(1)) == (void*)(1));
      else
	return (game_ia_easy());
    }
  return (0);
}
